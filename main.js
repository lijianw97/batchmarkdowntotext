const path = require('path');
const removeMd = require('remove-markdown');
const {readFileSync} = require('fs');
const fs = require('fs');

const mdFolderPath = "./markdowns/"
const txtFolderPath = "./texts/"
const isTarget = fileName => {
  const fullPath = path.join(mdFolderPath, fileName)
  return fs.lstatSync(fullPath).isFile() && fullPath.endsWith('.md');
};

function syncReadFile(filename) {
  const contents = readFileSync(filename, 'utf-8');
  return contents;
}

function convertToTxt(fileName) {
  const fullMdPath = path.join(mdFolderPath, fileName);
  const fullTxtPath = path.join(txtFolderPath, fileName) + ".txt";
  const markdownContent = syncReadFile(fullMdPath);
  const plainText = removeMd(markdownContent); // plainText is now 'This is a heading\n\nThis is a paragraph with a link in it.'
  fs.writeFileSync(fullTxtPath, plainText);
}

fs.readdirSync(mdFolderPath)
  .filter(isTarget)
  .map(fileName => {
    convertToTxt(fileName)
    console.log(`${fileName} converted`)
  })
