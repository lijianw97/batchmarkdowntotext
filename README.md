# batchMarkdownToText

## prerequisite: 
install `npm`
and run `npm install remove-markdown`
## step 1: download
urls.txt contains list of urls of the md files to download and convert

markdowns/ will contain temporary md files downloaded from the urls. 

run 
`cd markdowns;wget -nc -i ../urls.txt` 
to download all markdown files

## step 2: read and convert

go back to root directory of this repo and run 
`node main.js`.

texts/ will contain converted clean texts
